import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  DatePickerIOS,
  Modal,
  Button
} from 'react-native';
import styles from './styles';

export default class DatePicker extends React.Component {
  static defaultProps = {
    mode: 'date',
    locale: 'pt_BR',
    selectedDate: new Date()
  };

  static propTypes = {
    mode: PropTypes.oneOf(['date', 'time']),
    locale: PropTypes.string,
    selectedDate: PropTypes.instanceOf(Date)
  };

  state = {
    mode: this.props.mode,
    selectedDate: this.props.selectedDate,
    visible: false
  };

  onChange = () => null;

  handleToggle(mode='date', onChange) {
    if(onChange && typeof onChange === 'function')
      this.onChange = onChange;
    this.setState({ mode, visible: !this.state.visible });
  }

  handleOnDateChange(value) {
    this.setState({ selectedDate: value });
  }

  handleonDateSelected() {
    this.handleToggle();
    this.onChange(this.state.selectedDate);
  }

  render() {
    const {
      mode,
      selectedDate,
      visible
    } = this.state;
    return (
      <Modal
        visible={visible}
        onRequestClose={() => null}
        animationType="slide"
        transparent
      >
        <View style={styles.pickerViewContainer}>
          <View style={styles.pickerView}>
            <View style={styles.pickerViewTop}>
              <Button title="OK" onPress={() => this.handleonDateSelected()} />
            </View>
            <View style={styles.pickerViewBottom}>
              <DatePickerIOS
                mode={mode}
                locale={this.props.locale}
                date={selectedDate}
                onDateChange={value => this.handleOnDateChange(value)}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}
