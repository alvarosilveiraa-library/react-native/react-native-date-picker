import React from 'react';
import PropTypes from 'prop-types';
import {
  DatePickerAndroid,
  TimePickerAndroid
} from 'react-native';

export default class DatePicker extends React.Component {
  static defaultProps = {
    mode: 'date',
    selectedDate: new Date()
  };

  static propTypes = {
    mode: PropTypes.oneOf(['date', 'time']),
    selectedDate: PropTypes.instanceOf(Date)
  };

  state = {
    mode: this.props.mode,
    visible: false
  };

  onChange = () => null;

  componentDidUpdate(prevState) {
    if(!prevState.visible && this.state.visible) {
      const mode = this.state.mode;
      if(mode === 'date')
        this.renderDatePicker();
      else if(mode === 'time')
        this.renderTimePicker();
    }
  }

  handleToggle(mode='date', onChange) {
    if(typeof onChange === 'function')
      this.onChange = onChange;
    this.setState({ mode, visible: !this.state.visible });
  }

  handleOnDateChange(value) {
    this.handleToggle();
    this.onChange(value);
  }

  async renderTimePicker() {
    const { action, hour, minute } = await TimePickerAndroid.open({
      hour: this.props.selectedDate.getHours(),
      minute: this.props.selectedDate.getMinutes(),
      is24Hour: true
    });
    if(action !== TimePickerAndroid.dismissedAction) {
      const today = new Date();
      const selectedDate = new Date(
        today.getFullYear(),
        today.getMonth(),
        today.getDate(),
        hour,
        minute,
        0
      );
      this.handleOnDateChange(selectedDate);
    }else this.handleToggle();
  }

  async renderDatePicker() {
    const {
      action,
      year,
      month,
      day,
    } = await DatePickerAndroid.open({
      date: this.props.selectedDate,
    });
    if(action !== DatePickerAndroid.dismissedAction) {
      const selectedDate = new Date(year, month, day);
      this.handleOnDateChange(selectedDate);
    }else this.handleToggle();
  }

  render() {
    return null;
  }
}
