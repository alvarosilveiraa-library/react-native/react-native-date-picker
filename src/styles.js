import {
  StyleSheet,
  Platform,
  Dimensions
} from 'react-native';

const height = Dimensions.get('window').height;

export default StyleSheet.create({
  ...Platform.select({
    ios: {
      pickerViewContainer: {
        paddingTop: height / 1.5,
      },
      pickerView: {
        height: height - (height / 1.5),
      },
      pickerViewTop: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        height: 40,
        borderTopWidth: 0.5,
        borderTopColor: '#919498',
        backgroundColor: '#EFF1F2',
      },
      pickerViewBottom: {
        flex: 1,
        backgroundColor: '#D0D4DB',
      },
    },
  }),
});
